export function security(kibana) {
  return new kibana.Plugin({
    name: 'security',
    id: 'security',
    configPrefix: 'xpack.security',
    config(Joi) {
      return Joi.object({
        authProviders: Joi.array()
          .items(Joi.string())
          .default(['basic']),
        enabled: Joi.boolean().default(true),
        cookieName: Joi.string().default('sid'),
        encryptionKey: Joi.string(),
        sessionTimeout: Joi.number()
          .allow(null)
          .default(null),
        secureCookies: Joi.boolean().default(false),
        public: Joi.object({
          protocol: Joi.string().valid(['http', 'https']),
          hostname: Joi.string().hostname(),
          port: Joi.number()
            .integer()
            .min(0)
            .max(65535),
        }).default(),
      }).default();
    },
  });
}
