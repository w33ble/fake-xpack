export function graph(kibana) {
  return new kibana.Plugin({
    name: 'graph',
    id: 'graph',
    configPrefix: 'xpack.graph',
    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
        canEditDrillDownUrls: Joi.boolean().default(true),
        savePolicy: Joi.string()
          .valid(['config', 'configAndDataWithConsent', 'configAndData', 'none'])
          .default('configAndData'),
      }).default();
    },
  });
}
