export function ml(kibana) {
  return new kibana.Plugin({
    name: 'ml',
    id: 'ml',
    configPrefix: 'xpack.ml',
    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },
  });
}
