export function reporting(kibana) {
  return new kibana.Plugin({
    name: 'reporting',
    id: 'reporting',
    configPrefix: 'xpack.reporting',
    async config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
        kibanaServer: Joi.object({
          protocol: Joi.string().valid(['http', 'https']),
          hostname: Joi.string(),
          port: Joi.number().integer(),
        }).default(),
        queue: Joi.object({
          indexInterval: Joi.string().default('week'),
          pollInterval: Joi.number()
            .integer()
            .default(3000),
          pollIntervalErrorMultiplier: Joi.number()
            .integer()
            .default(10),
          timeout: Joi.number()
            .integer()
            .default(30000),
        }).default(),
        capture: Joi.object({
          record: Joi.boolean().default(false),
          zoom: Joi.number()
            .integer()
            .default(2),
          viewport: Joi.object({
            width: Joi.number()
              .integer()
              .default(1950),
            height: Joi.number()
              .integer()
              .default(1200),
          }).default(),
          timeout: Joi.number()
            .integer()
            .default(20000), //deprecated
          loadDelay: Joi.number()
            .integer()
            .default(3000),
          settleTime: Joi.number()
            .integer()
            .default(1000), //deprecated
          concurrency: Joi.number()
            .integer()
            .default(appConfig.concurrency), //deprecated
          browser: Joi.object({
            type: Joi.any()
              .valid('phantom', 'chromium')
              .default(await getDefaultBrowser()),
            autoDownload: Joi.boolean().when('$dev', {
              is: true,
              then: Joi.default(true),
              otherwise: Joi.default(false),
            }),
            chromium: Joi.object({
              disableSandbox: Joi.boolean().default(await getDefaultChromiumSandboxDisabled()),
              proxy: Joi.object({
                enabled: Joi.boolean().default(false),
                server: Joi.string()
                  .uri({ scheme: ['http', 'https'] })
                  .when('enabled', {
                    is: Joi.valid(false),
                    then: Joi.valid(null),
                    else: Joi.required(),
                  }),
                bypass: Joi.array()
                  .items(Joi.string().regex(/^[^\s]+$/))
                  .when('enabled', {
                    is: Joi.valid(false),
                    then: Joi.valid(null),
                    else: Joi.default([]),
                  }),
              }).default(),
              maxScreenshotDimension: Joi.number()
                .integer()
                .default(1950),
            }).default(),
          }).default(),
        }).default(),
        csv: Joi.object({
          maxSizeBytes: Joi.number()
            .integer()
            .default(1024 * 1024 * 10), // bytes in a kB * kB in a mB * 10
          scroll: Joi.object({
            duration: Joi.string()
              .regex(/^[0-9]+(d|h|m|s|ms|micros|nanos)$/, { name: 'DurationString' })
              .default('30s'),
            size: Joi.number()
              .integer()
              .default(500),
          }).default(),
        }).default(),
        encryptionKey: Joi.string(),
        roles: Joi.object({
          allow: Joi.array()
            .items(Joi.string())
            .default(['reporting_user']),
        }).default(),
        index: Joi.string().default('.reporting'),
        poll: Joi.object({
          jobCompletionNotifier: Joi.object({
            interval: Joi.number()
              .integer()
              .default(10000),
            intervalErrorMultiplier: Joi.number()
              .integer()
              .default(5),
          }).default(),
          jobsRefresh: Joi.object({
            interval: Joi.number()
              .integer()
              .default(5000),
            intervalErrorMultiplier: Joi.number()
              .integer()
              .default(5),
          }).default(),
        }).default(),
      }).default();
    },
  });
}
