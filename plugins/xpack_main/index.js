export function xpackMain(kibana) {
  return new kibana.Plugin({
    require: [],
    name: 'xpack_main',
    id: 'xpack_main',
    configPrefix: 'xpack.xpack_main',
    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
        telemetry: Joi.object({
          enabled: Joi.boolean().default(true),
          url: Joi.when('$dev', {
            is: true,
            then: Joi.string().default('https://telemetry-staging.elastic.co/xpack/v1/send'),
            otherwise: Joi.string().default('https://telemetry.elastic.co/xpack/v1/send'),
          }),
        }).default(),
        xpack_api_polling_frequency_millis: Joi.number().default(
          XPACK_INFO_API_DEFAULT_POLL_FREQUENCY_IN_MILLIS
        ),
      }).default();
    },
  });
}
