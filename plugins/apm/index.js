export function apm(kibana) {
  return new kibana.Plugin({
    name: 'apm',
    id: 'apm',
    configPrefix: 'xpack.apm',
    config(Joi) {
      return Joi.object({
        ui: Joi.object({
          enabled: Joi.boolean().default(true),
        }).default(),
        enabled: Joi.boolean().default(true),
        indexPattern: Joi.string().default('apm*'),
        minimumBucketSize: Joi.number().default(15),
        bucketTargetCount: Joi.number().default(27),
      }).default();
    },
  });
}
