import { xpackMain } from './plugins/xpack_main';
import { apm } from './plugins/apm';
import { graph } from './plugins/graph';
import { ml } from './plugins/ml';
import { monitoring } from './plugins/monitoring';
import { reporting } from './plugins/reporting';
import { security } from './plugins/security';

const fakeXpack = kibana => {
  return new kibana.Plugin({
    name: 'fake_xpack',
    id: 'fake_xpack',
    init() {
      console.log('FAKE X-PACK IN FULL EFFECT! [xpack_main]');
    },
  });
};

export default function(kibana) {
  return [fakeXpack, xpackMain(kibana), apm, graph, ml, monitoring, reporting, security];
}
